<?php

// define the function "haveFun" so we can use it later
function haveFun() {

	// grab the data that we enter into the HTML form
	// by storing them inside variables
	$n = $_GET["val1"];
	$a = $_GET["val2"];

	// use a switch (if this, then this, or else) to have fun with the name and age submitted
	switch ($a) {
	
		// if the variable age is less than 0 (negative value),
		// then echo "Hey, $name, are you still in a womb?"
		case ($a <= 0):
			echo 'Hey, ' . $n . ', are you still in a womb?';
			
			// use a break to end the case for if the age is a negative value
			break;
		
		// if the user is ages 0 thru 21,
		// then echo "$name is fairly young, naive, and stupid."
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
		case 20:
		case 21:
			echo $n . ' is fairly young, naive, and stupid.';
			
			// use a break to end the case for if the age is 0 thru 21
			break;
		
		// if the user is ages 22 thru 99,
		// then echo "Aging pretty nicely!"
		case ($a >= 22 && $a <= 99):
			echo 'Aging pretty nicely!';
			
			// use a break to end the case for if the age is 22 thru 99
			break;
		
		// if the user is ages 100 thru 199,
		// then echo "Being the old human that $name is, please tell me his age does not match that of a turtle's."
		case ($a >= 100 && $a <= 199):
			echo 'Being the old human that ' . $n . ' is, please tell me his age does not match that of a turtle\'s.';
			
			// use a break to end the case for if the age is 100 thru 199
			break;
			
		// if the user is ages 200 thru 13094,
		// then echo "This is just... just... BS!"
		case ($a >= 200 && $a <= 13094):
			echo 'This is just... just... BS!';
			
			// use a break to end the case for if the age is 200 thru 13094
			break;
		
		// if the user is ages 13095 or above,
		// then echo "I do not care about anything pertaining to $name."
		case ($a >= 13095):
			echo 'I do not care about anything pertaining to ' . $n . '.';
			
			// use a break to end the case for if the age is 13095 or greater
			break;
	
	// end the switch
	}
	
//end the "haveFun" function
}

?>

<!-- load the form to ask for a name and an age -->
<html>
	<head>
		<!-- don't forget to make the title nice! -->
		<title>Make this a nice little title.</title>
	</head>
	
	</body>
		<!-- here's the form -->
		<form action="" method="get">
			
			<!-- ask for a name and save it to an ID of "val1" -->
			<input type="text" placeholder="What's your name?" name="val1" id="val1"></input>
			<br></br>

			<!-- ask for an age and save it to an ID of "val2" -->
			<input type="text" placeholder="What's your age?" name="val2" id="val2"></input>
			<br></br>
			
			<!-- submit the data -->
			<input type="submit" value="OK, submit my info now!"></input>
		</form>
	</body>

</html>

<?php
// check whether the form was submitted,
// if yes, then proceed to the "haveFun" function
if( isset($_GET['val2']) )
{
    //then you can use them in the PHP function "haveFun" written at the top of this page 
    $result = haveFun();
}

	// print the result below the form
	if( isset($result) ) echo $result;

?>